<?php

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    private $cacheDir;
    private $logDir;

    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            new Acme\Bundle\ApplicationBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles = array_merge($bundles, [
                new Symfony\Bundle\DebugBundle\DebugBundle(),
                new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle(),
                new Sensio\Bundle\DistributionBundle\SensioDistributionBundle(),
                new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle(),
            ]);
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $configDir = $this->getConfigDir();
        $environment = $this->getEnvironment();
        $loader->load("{$configDir}/parameters_{$environment}.yml");
        $loader->load("{$configDir}/config_{$environment}.yml");
    }

    protected function getKernelParameters()
    {
        return array_merge(parent::getKernelParameters(), [
            'kernel.project_dir'   => $this->getProjectDir(),
            'kernel.config_dir'    => $this->getConfigDir(),
            'kernel.resources_dir' => $this->getResourcesDir(),
            'kernel.public_dir'    => $this->getPublicDir(),
            'kernel.temp_dir'      => $this->getTempDir(),
        ]);
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return $this->cacheDir ?: (
            $this->cacheDir = "{$this->getTempDir()}/cache/{$this->getEnvironment()}"
        );
    }

    public function getLogDir()
    {
        return $this->logDir ?: (
            $this->logDir = "{$this->getTempDir()}/logs"
        );
    }

    private function getProjectDir()
    {
        return "{$this->rootDir}/..";
    }

    private function getTempDir()
    {
        return "{$this->getProjectDir()}/var";
    }

    private function getConfigDir()
    {
        return "{$this->getProjectDir()}/etc";
    }

    private function getResourcesDir()
    {
        return "{$this->getProjectDir()}/res";
    }

    private function getPublicDir()
    {
        return "{$this->getProjectDir()}/public";
    }
}
