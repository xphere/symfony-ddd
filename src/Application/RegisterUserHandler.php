<?php

namespace Acme\Application;

use Acme\Authentication\Password;
use Acme\Authentication\PasswordEncoder;
use Acme\Authentication\SaltGenerator;
use Acme\Authentication\User;
use Acme\Authentication\Username;
use Acme\Authentication\UserRepository;

class RegisterUserHandler
{
    private $userRepository;
    private $passwordEncoder;
    private $saltGenerator;

    public function __construct(
        UserRepository $userRepository,
        PasswordEncoder $passwordEncoder,
        SaltGenerator $saltGenerator
    ) {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->saltGenerator = $saltGenerator;
    }

    public function handle(RegisterUser $user)
    {
        $user = User::register(
            $this->username($user->username),
            $this->password($user->password)
        );

        $this->userRepository->add($user);
    }

    private function username($value)
    {
        return new Username($value);
    }

    private function password($plainPassword)
    {
        return Password::generate(
            $this->passwordEncoder,
            $plainPassword,
            $this->saltGenerator->generate()
        );
    }
}
