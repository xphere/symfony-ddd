<?php

namespace Acme\Bundle;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;

trait Bundle
{
    use ContainerAwareTrait;

    public function build(ContainerBuilder $container)
    {
    }

    public function getContainerExtension()
    {
    }

    public function boot()
    {
    }

    public function shutdown()
    {
    }

    public function getParent()
    {
    }
}
