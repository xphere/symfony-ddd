<?php

namespace Acme\Bundle\Request;

use Symfony\Component\HttpFoundation\Request;

class Body implements \ArrayAccess
{
    private $content;

    public static function fromString($content, $contentType)
    {
        switch ($contentType) {
            case 'txt': return new self($content);
            case 'json': return self::fromJson($content);
        }

        throw new \Exception('Can\'t parse request body: type unknown');
    }

    public static function fromJson($content)
    {
        $result = json_decode($content, true);

        if ($result !== null || json_last_error() === \JSON_ERROR_NONE) {
            return new self($result);
        }

        throw new \Exception(sprintf(
            'Error parsing json body: %s',
            json_last_error_msg()
        ));
    }

    public static function fromRequest(Request $request)
    {
        return self::fromString(
            $request->getContent(),
            $request->getContentType()
        );
    }

    public function __construct($content)
    {
        $this->content = $content;
    }

    public function content()
    {
        return $this->content;
    }

    public function get($key, $default = null)
    {
        if (!is_array($this->content) && !$this->content instanceof \ArrayAccess) {
            throw new \Exception('Incorrect body content format: needs an array');
        }

        if (isset($this->content[$key])) {
            return $this->content[$key];
        }

        return $default;
    }

    public function offsetExists($offset)
    {
        return isset($this->content[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->content[$offset];
    }

    public function offsetSet($offset, $value)
    {
        throw new \Exception('Body content is immutable');
    }

    public function offsetUnset($offset)
    {
        throw new \Exception('Body content is immutable');
    }
}
