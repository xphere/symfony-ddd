<?php

namespace Acme\Bundle\Request;

use SimpleBus\Message\Bus\MessageBus;

class CommandBus
{
    private $bus;

    public function __construct(MessageBus $bus)
    {
        $this->bus = $bus;
    }

    public function handle($command)
    {
        $this->bus->handle($command);
    }
}
