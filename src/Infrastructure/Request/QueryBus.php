<?php

namespace Acme\Bundle\Request;

use SimpleBus\Message\Handler\Resolver\MessageHandlerResolver;

class QueryBus
{
    private $messageHandlerResolver;

    public function __construct(MessageHandlerResolver $messageHandlerResolver)
    {
        $this->messageHandlerResolver = $messageHandlerResolver;
    }

    public function execute($query)
    {
        $handler = $this->messageHandlerResolver->resolve($query);

        return $handler($query);
    }
}
