<?php

namespace Acme\Bundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Xphere\Tag;

class ApplicationBundle implements BundleInterface
{
    use Bundle;

    public function getName()
    {
        return 'ApplicationBundle';
    }

    public function getNamespace()
    {
        return __NAMESPACE__;
    }

    public function getPath()
    {
        return __DIR__;
    }

    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(
            Tag\CompilerPassBuilder::create()
                ->byTag('is_argument_value')
                ->asLazy()
                ->indexedBy(
                    new Tag\By\ClassName(),
                    new Tag\Index\ThrowIfMultiple()
                )
                ->injectTo('core.class_argument_resolver',
                    new Tag\Inject\Constructor\ReplaceArgument('mapping')
                )
                ->build()
        );
    }
}
