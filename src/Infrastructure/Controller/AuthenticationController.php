<?php

namespace Acme\Bundle\Controller;

use Acme\Application\UserLogin;
use Acme\Bundle\Request\Body;
use Acme\Bundle\Request\CommandBus;
use Symfony\Component\HttpFoundation\Response;

class AuthenticationController
{
    public function registerAction(Body $body, CommandBus $bus)
    {
        $bus->handle(
            new UserLogin(
                $body['username'],
                $body['password']
            )
        );

        return new Response();
    }
}
