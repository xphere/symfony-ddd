<?php

namespace Acme\Bundle\Service;

use Acme\Authentication\SaltGenerator;

class RandomBytesSaltGenerator implements SaltGenerator
{
    private $length;

    public function __construct($length = 40)
    {
        $this->length = $length;
    }

    public function generate()
    {
        return random_bytes($this->length);
    }
}
