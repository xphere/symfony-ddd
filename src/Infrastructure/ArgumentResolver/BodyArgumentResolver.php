<?php

namespace Acme\Bundle\ArgumentResolver;

use Acme\Bundle\Request\Body;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class BodyArgumentResolver implements ArgumentValueResolverInterface
{
    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return $argument->getType() === Body::class;
    }

    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        yield Body::fromRequest($request);
    }
}
