<?php

namespace Acme\Authentication;

interface UserRepository
{
    public function add(User $user);
}
