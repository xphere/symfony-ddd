<?php

namespace Acme\Authentication\Event;

use Acme\Authentication;

class UserWasRegistered
{
    public $id;
    public $username;
    public $password;

    public function __construct(
        Authentication\UserId $id,
        Authentication\Username $username,
        Authentication\Password $password
    ) {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
    }
}
