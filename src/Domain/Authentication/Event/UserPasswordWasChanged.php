<?php

namespace Acme\Authentication\Event;

use Acme\Authentication;

class UserPasswordWasChanged
{
    public $id;
    public $password;

    public function __construct(
        Authentication\UserId $id,
        Authentication\Password $password
    ) {
        $this->id = $id;
        $this->password = $password;
    }
}
