<?php

namespace Acme\Authentication;

class Password
{
    private $password;
    private $salt;

    public static function generate(PasswordEncoder $encoder, $plainPassword, $salt)
    {
        return new self(
            $encoder->encode($plainPassword, $salt),
            $salt
        );
    }

    private function __construct($encodedPassword, $salt)
    {
        $this->password = $encodedPassword;
        $this->salt = $salt;
    }
}
