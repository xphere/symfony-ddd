<?php

namespace Acme\Authentication;

use Acme\AggregateRoot;

class User
{
    use AggregateRoot;

    private $id;
    private $password;
    private $username;

    public static function register(UserId $id, Username $username, Password $password)
    {
        $self = new self($id, $username, $password);

        $self->recordThat(
            new Event\UserWasRegistered($id, $username, $password)
        );

        return $self;
    }

    private function __construct(UserId $id, Username $username, Password $password)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
    }

    public function changePassword(Password $password)
    {
        $this->password = $password;

        $this->recordThat(
            new Event\UserPasswordWasChanged($this->id, $password)
        );
    }
}
