<?php

namespace Acme\Authentication;

use Acme\Common\ValueObject;

class UserId
{
    use ValueObject;
}
