<?php

namespace Acme\Authentication;

interface SaltGenerator
{
    public function generate();
}
