<?php

namespace Acme\Authentication;

interface PasswordEncoder
{
    public function encode($plainPassword, $salt);
}
