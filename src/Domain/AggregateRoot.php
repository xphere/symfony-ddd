<?php

namespace Acme;

trait AggregateRoot
{
    private $recordedEvents = [];

    public function releaseRecordedEvents()
    {
        $recordedEvents = $this->recordedEvents;
        $this->recordedEvents = [];

        return $recordedEvents;
    }

    private function recordThat($event)
    {
        $this->recordedEvents[] = $event;
    }
}
