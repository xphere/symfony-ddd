<?php

namespace Acme;

trait ValueObject
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function equals($other)
    {
        return $other instanceof self && $this->value === $other->value;
    }
}
