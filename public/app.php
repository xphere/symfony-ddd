<?php

use Symfony\Component\HttpFoundation\Request;

require __DIR__ . '/../var/bootstrap.php.cache';

$request = Request::createFromGlobals();

$kernel = new Kernel('prod', false);
$kernel->loadClassCache();

$kernel = new Cache($kernel);

$response = $kernel->handle($request);
$response->send();

$kernel->terminate($request, $response);
