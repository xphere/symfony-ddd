<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;

umask(0002);

require __DIR__ . '/../boot/autoload.php';

Debug::enable();
$request = Request::createFromGlobals();
$kernel = new Kernel('dev', true);

$response = $kernel->handle($request);
$response->send();

$kernel->terminate($request, $response);
