Simple Symfony
==============

- Directory structure following [FHS](https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard).
    - `/bin`: executable files.
    - `/boot`: kernel and infrastructure.
    - `/etc`: configuration.
    - `/lib`: vendors.
    - `/public`: public files.
    - `/src`: source code.
    - `/var`: temporary files, mainly cache and logs.
- Avoid loading `bootstrap.php.cache` and `classes.php` when not required.
- Requires explicit `etc/parameters_prod.yml` to be included before deploy.
